package org.bitbucket.rajibpsarma.bean_using_xml_config;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyApp {
	public static void main(String[] args) {
		// Load spring config
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("org/bitbucket/rajibpsarma/bean_using_xml_config/applicationContext.xml");
		
		// get the bean
		CoachI coach = context.getBean("myCoach", CoachI.class);
		
		// invoke method
		System.out.println(coach.getDailyWorkout());
		
		// close the context
		context.close();
	}
}
