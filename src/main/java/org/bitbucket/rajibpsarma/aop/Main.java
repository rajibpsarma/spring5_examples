package org.bitbucket.rajibpsarma.aop;

import java.util.Date;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/*
 * It explores various types of AOP Advices
 * @author: Rajib Sarma
 */

/*
This program outputs something like this, to the console:

BEFORE ADVICE example:
ASPECT(Before advice):Invoking CustomerService.createCustomer(..)
Creating customer : Id=1, Name=R P Sarma

ASPECT(Before advice):Invoking OrderService.createOrder(..)
Creating Order:Order [id=100, customer=Rajib, date=Sat Jul 11 14:46:42 IST 2020]
-----------------------------------------------
AFTER RETURNING ADVICE example:
Getting Customer Name ...
ASPECT(After Returning):'getCustomerName()' returns: 'Rajib'

Getting order details ...
ASPECT(After Returning):'getOrder()' returns: 'Order [id=100, customer=Rajib, date=Sat Jul 11 14:46:42 IST 2020]'
-----------------------------------------------
AFTER THROWING ADVICE example:
Getting Customer Name ...
ASPECT(After Throwing): Error executing 'getCustomerName()': Customer with number '0' is not found !

Getting order details ...
ASPECT(After Throwing): Error executing 'getOrder()': Order with date 'null' is Not Found
-----------------------------------------------
AROUND ADVICE example:
ASPECT(Around):Invoking CustomerService.updateCustomer(..)
Updating customer : Id=420, Name=Rajib Sarma
ASPECT(Around):CustomerService.updateCustomer(..) returns:true
ASPECT(Around):Time consumed:CustomerService.updateCustomer(..):102 mSec

ASPECT(Around):Invoking OrderService.updateOrder(..)
Updating order:Order [id=100, customer=Rajib, date=Sat Jul 11 14:46:42 IST 2020]
ASPECT(Around):Error executing 'OrderService.updateOrder(..)': Error updating order:Order [id=100, customer=Rajib, date=Sat Jul 11 14:46:42 IST 2020]
ASPECT(Around):OrderService.updateOrder(..) returns:null
ASPECT(Around):Time consumed:OrderService.updateOrder(..):101 mSec
-----------------------------------------------
*/
public class Main {

	public static void main(String[] args) throws CustomerNotFoundException {
		String separator = "-----------------------------------------------";
		// Load spring config
		AnnotationConfigApplicationContext  context = 
				new AnnotationConfigApplicationContext (Config.class);
		
		// get the beans
		CustomerService customer = context.getBean("customerService", CustomerService.class);
		OrderService order = context.getBean("orderService", OrderService.class);
		
		// invoke method on beans
		System.out.println("BEFORE ADVICE example:");
		customer.createCustomer(1, "R P Sarma");
		System.out.println("");
		order.createOrder(new Order());
		System.out.println(separator);
		
		System.out.println("AFTER RETURNING ADVICE example:");
		// Get a Customer successfully
		customer.getCustomerName(1);
		System.out.println("");
		// Get order details
		order.getOrder(new Date());
		System.out.println(separator);
		
		System.out.println("AFTER THROWING ADVICE example:");
		// Throws exception while getting customer
		try {
			customer.getCustomerName(0);
		} catch(CustomerNotFoundException e) {}
		System.out.println("");
		try {
			order.getOrder(null);
		}catch(Exception ex) {}
		System.out.println(separator);
		
		System.out.println("AROUND ADVICE example:");
		// Update Customer successfully
		customer.updateCustomer(420, "Rajib Sarma");
		System.out.println("");
		// Update order details, exception thrown
		order.updateOrder(new Order());
		System.out.println(separator);
		
		// close the context
		context.close();
	}
}