package org.bitbucket.rajibpsarma.aop;

public class CustomerNotFoundException extends Exception {
	public CustomerNotFoundException() {}
	public CustomerNotFoundException(String errorMsg) {
		super(errorMsg);
	}
}