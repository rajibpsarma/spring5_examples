package org.bitbucket.rajibpsarma.aop;

import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;

@Component
public class CustomerService {
	// Create customer
	public void createCustomer(int custId, String custName) {
		System.out.println("Creating customer : Id=" + custId + ", Name=" + custName);
	}
	
	// update customer
	public boolean updateCustomer(int custId, String custName) {
		System.out.println("Updating customer : Id=" + custId + ", Name=" + custName);
		
		// Simulate some delay
		try {
			TimeUnit.MILLISECONDS.sleep(100);
		}catch(InterruptedException e) {}
		
		return true;
	}
	
	// Get customer name
	public String getCustomerName(int number) throws CustomerNotFoundException {
		System.out.println("Getting Customer Name ...");
		if(number == 0) {
			throw new CustomerNotFoundException("Customer with number '" + number + "' is not found !");
		}
		return "Rajib";
	}
}