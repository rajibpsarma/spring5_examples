package org.bitbucket.rajibpsarma.aop;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;

@Component
public class OrderService {
	// Create order
	public void createOrder(Order order) {
		System.out.println("Creating Order:" + order);
	}
	
	// Exception occurs while updating an order
	public void updateOrder(Order order) {
		System.out.println("Updating order:" + order);
		
		// Simulate some delay
		try {
			TimeUnit.MILLISECONDS.sleep(100);
		}catch(InterruptedException e) {}
		
		throw new RuntimeException("Error updating order:" + order);
	}
	
	// Get order
	public Order getOrder(Date date) {
		System.out.println("Getting order details ...");
		if(date == null) {
			throw new RuntimeException("Order with date '" + date + "' is Not Found");
		}
		return new Order();
	}
}
