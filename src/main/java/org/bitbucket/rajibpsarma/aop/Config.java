package org.bitbucket.rajibpsarma.aop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("org.bitbucket.rajibpsarma.aop")
@EnableAspectJAutoProxy
public class Config {
}