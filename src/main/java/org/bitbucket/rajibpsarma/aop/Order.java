package org.bitbucket.rajibpsarma.aop;

import java.util.Date;

public class Order {
	private int id=100;
	private String customer="Rajib";
	private Date date=new Date();
	
	@Override
	public String toString() {
		return "Order [id=" + id + ", customer=" + customer + ", date=" + date + "]";
	}
}