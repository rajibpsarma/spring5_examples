package org.bitbucket.rajibpsarma.aop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/*
 * It explores various types of AOP Advices
 * @author: Rajib Sarma
 */
@Component
@Aspect
public class AOPAspect {
	// The following before advice is called before createCustomer()
	// @Before("execution(public void createCustomer())")
	
	// Invoke the before advice before all create*() methods
	@Before("execution(* org.bitbucket.rajibpsarma.aop.*.create*(..))")
	public void beforeAdvice(JoinPoint joinPoint) {
		System.out.println("ASPECT(Before advice):Invoking " + joinPoint.getSignature().toShortString());
	}
	
	
	// Invoked once getCustomerName(int) completes successfully
	//@AfterReturning("execution(public String getCustomerName(int))")
	
	// Apply to all get methods.
	// returning is the value returned by the method.
	@AfterReturning(pointcut = "execution(* get*(..))",
			returning = "retVal")
	public void afterReturningAdvice(JoinPoint joinPoint, Object retVal) {
		String methodName = joinPoint.getSignature().getName() + "()";
		System.out.println("ASPECT(After Returning):" + "'" + methodName + "' returns: '" + retVal + "'");
	}
	
	
	// Invoked when any get*() method (getCustomer(), getOrder()) throws exception
	@AfterThrowing(pointcut = "execution(* org.bitbucket.rajibpsarma.aop.*.get*(..))",
			throwing = "ex")
	public void afterThrowingAdvice(JoinPoint joinPoint, Exception ex) {
		String methodName = joinPoint.getSignature().getName() + "()";
		System.out.println("ASPECT(After Throwing): Error executing '" + methodName + "': " + ex.getMessage());
	}
	

	// Around advice, used to determine the time taken by a method
	// Applied to all update methods.
	@Around("execution(* *.update*(..))")
	public Object aroundAdvice(ProceedingJoinPoint jp) throws Throwable {
		String prefix = "ASPECT(Around):";
		String method = jp.getSignature().toShortString();
		System.out.println(prefix + "Invoking " + method);
		long startTime = System.currentTimeMillis();
		
		// Execute the method.
		// For exceptions, log the exception and return null value.
		Object result = null;
		try {
			result = jp.proceed();
		}
		catch(Exception ex) {
			System.out.println(prefix + "Error executing '" + method + "': " + ex.getMessage());
			// throw ex; // Can throw it to the caller also
		}
		System.out.println(prefix + method + " returns:" + result);
		
		long endTime = System.currentTimeMillis();
		System.out.println(prefix + "Time consumed:" + method + ":" + (endTime-startTime) + " mSec");
		
		return result;
	}
}
