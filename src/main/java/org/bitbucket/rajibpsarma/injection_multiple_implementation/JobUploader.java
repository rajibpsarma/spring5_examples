package org.bitbucket.rajibpsarma.injection_multiple_implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class JobUploader implements Uploader {
	@Autowired
	@Qualifier("jobUploaderService")
	private UploaderService service;
	public void upload() {
		System.out.println("Uploading Jobs ...");
		this.service.upload();
	}
}
