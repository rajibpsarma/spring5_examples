package org.bitbucket.rajibpsarma.injection_multiple_implementation;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.bitbucket.rajibpsarma.injection_multiple_implementation")
public class Config {
}