package org.bitbucket.rajibpsarma.injection_multiple_implementation;

public interface Uploader {
	void upload();
}