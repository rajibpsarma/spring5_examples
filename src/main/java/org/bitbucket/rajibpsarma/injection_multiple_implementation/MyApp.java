package org.bitbucket.rajibpsarma.injection_multiple_implementation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MyApp {
	public static void main(String[] args) {
		// Load spring config
		AnnotationConfigApplicationContext  context = 
				new AnnotationConfigApplicationContext (Config.class);
		
		// get the job uploader bean
		Uploader jobUploader = context.getBean("jobUploader", Uploader.class);
		Uploader candUploader = context.getBean("candidateUploader", Uploader.class);
		
		// invoke method
		jobUploader.upload();
		candUploader.upload();
		
		// close the context
		context.close();
	}
}
