package org.bitbucket.rajibpsarma.injection_multiple_implementation;

import org.springframework.stereotype.Component;

@Component
public class JobUploaderService implements UploaderService {
	public void upload() {
		System.out.println("Uploading Jobs using service ...");
	}
}
