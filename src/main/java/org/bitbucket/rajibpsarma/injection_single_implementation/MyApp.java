package org.bitbucket.rajibpsarma.injection_single_implementation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MyApp {
	public static void main(String[] args) {
		// Load spring config
		AnnotationConfigApplicationContext  context = 
				new AnnotationConfigApplicationContext (Config.class);
		
		// get the job uploader bean
		Uploader jobUploader = context.getBean("jobUploader", Uploader.class);
		
		// invoke method
		jobUploader.upload();
		
		// close the context
		context.close();
	}
}
