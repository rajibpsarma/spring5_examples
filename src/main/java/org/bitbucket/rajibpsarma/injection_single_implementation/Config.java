package org.bitbucket.rajibpsarma.injection_single_implementation;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.bitbucket.rajibpsarma.injection_single_implementation")
public class Config {
}