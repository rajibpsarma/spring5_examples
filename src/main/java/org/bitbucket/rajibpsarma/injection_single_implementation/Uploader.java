package org.bitbucket.rajibpsarma.injection_single_implementation;

public interface Uploader {
	void upload();
}