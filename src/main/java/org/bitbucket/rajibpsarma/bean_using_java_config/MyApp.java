package org.bitbucket.rajibpsarma.bean_using_java_config;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MyApp {
	public static void main(String[] args) {
		// Load spring config
		AnnotationConfigApplicationContext  context = 
				new AnnotationConfigApplicationContext (BaseballConfig.class);
		
		// get the bean
		CoachI coach = context.getBean("baseballCoach", CoachI.class);
		
		// invoke method
		System.out.println(coach.getDailyWorkout());
		
		// close the context
		context.close();
	}
}
