package org.bitbucket.rajibpsarma.bean_using_java_config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.bitbucket.rajibpsarma.bean_using_java_config")
public class BaseballConfig {
}