package org.bitbucket.rajibpsarma.bean_using_java_config;

import org.springframework.stereotype.Component;

@Component
public class BaseballCoach implements CoachI {
	public String getDailyWorkout() {
		return "Baseball coach daily workout";
	}
}
